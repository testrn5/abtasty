import { test } from "@playwright/test";
import { LoginPage } from "../pages/LoginPage";
import { ResetPasswordPage } from "../pages/ResetPasswordPage";

let loginPage: LoginPage;
let resetPasswordPage: ResetPasswordPage;

test.describe('Reset password tests', () => {
    test.beforeEach(async ({page}) => {
        loginPage = new LoginPage(page)
        resetPasswordPage = new ResetPasswordPage(page)

        await loginPage.open()
        await loginPage.followResetPassword()
        await resetPasswordPage.checkInitState()
    })

    test('Invalid email format', async ({page}) => {
        await resetPasswordPage.fillEmail('test')
        await resetPasswordPage.submitForm()
        await resetPasswordPage.seeEmailFormatErr()
    })
})
