import { Page } from '@playwright/test';
import { Control } from "./components/Control";

export class MfaRequiredPage {
    readonly page: Page

    readonly title: Control

    constructor(page: Page) {
        this.page = page

        this.title = new Control({page: page, loc: page.locator('//h2[text()="MFA is required to access this account"]')})
    }

    async checkInitState() {
        await this.title.waitForVisible()
    }
}
