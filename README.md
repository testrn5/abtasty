### Install required packages:
```shell
npm i
```

### Start tests:
```shell
BASE_URL='https://app2.abtasty.com' USER_EMAIL='test@test.com' USER_PWD='password' npx playwright test
```
