import {Locator, Page} from "@playwright/test";

export interface Options {
    page: Page,
    loc: Locator,
}
