import { Locator, Page } from '@playwright/test';
import { EmailInput } from "./components/inputs/Email";
import { Input } from "./components/Input";
import { Button } from "./components/Button";
import { Control } from "./components/Control";

export class LoginPage {
    private page: Page

    readonly emailInput: EmailInput
    readonly emailErrSection: Control

    readonly pwdInput: Input
    readonly submitBtn: Button
    readonly loginErrSection: Control

    readonly resetPasswordBtn: Button

    constructor(page: Page) {
        this.page = page

        this.emailInput = new EmailInput(page)
        this.emailErrSection = new Control({page: page, loc: page.getByTestId('emailErrorMessage')})

        this.pwdInput = new Input({page: page, loc: page.locator('#password')})
        this.submitBtn = new Button({page: page, loc: page.locator('#signInButton')})
        this.loginErrSection = new Control({page: page, loc: page.locator('#loginErrorMessage')})

        this.resetPasswordBtn = new Button({page: page, loc: page.locator('//a[@href="/reset-password"]')})
    }

    async open() {
        await this.page.goto('/login', { waitUntil: 'networkidle' })
    }

    async checkInitState() {
        await this.emailInput.waitForVisible()
        await this.pwdInput.waitForVisible()
    }

    async fillEmail(email: string) {
        await this.emailInput.fill(email)
    }

    async focusOnPassword() {
        await this.pwdInput.click()
    }

    async fillCredentials(email: string, password: string) {
        await this.fillEmail(email)
        await this.pwdInput.fill(password)
    }

    async seeEmailFormatErr() {
        await this.emailErrSection.waitForTextVisible('Please enter a valid email')
    }

    async seeLoginInvalidCredentialsError() {
        await this.loginErrSection.waitForTextVisible('Please enter a valid email or password')
    }

    async signIn() {
        await this.submitBtn.click()
    }

    async followResetPassword() {
        await this.resetPasswordBtn.click()
        await this.page.waitForLoadState('networkidle')
    }
}
