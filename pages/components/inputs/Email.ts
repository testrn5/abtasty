import { Page } from "@playwright/test";
import { Input } from "../Input";

export class EmailInput extends Input {
    constructor(page: Page) {
        super({
            page: page,
            loc: page.locator('#email')
        });
    }
}
