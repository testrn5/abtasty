import { Control } from "./Control";

export class Input extends Control {
    async fill(val: string) {
        await this.loc.fill(val)
    }
}
