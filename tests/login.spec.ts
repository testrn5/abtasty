import { test } from '@playwright/test';
import { LoginPage } from "../pages/LoginPage";
import { users } from "../data/constants";
import { MfaRequiredPage } from "../pages/MfaRequiredPage";

let loginPage: LoginPage
let mfaRequiredPage: MfaRequiredPage

test.describe('Login tests', () => {
    test.beforeEach(async ({page}) => {
        loginPage = new LoginPage(page)

        await loginPage.open()
        await loginPage.checkInitState()
    })

    test('Invalid email format', async () => {
        await loginPage.fillEmail('test')
        await loginPage.focusOnPassword()
        await loginPage.seeEmailFormatErr()
    })

    test('Invalid credentials', async () => {
        await loginPage.fillCredentials('test@test.com', 'ttt')
        await loginPage.signIn()
        await loginPage.seeLoginInvalidCredentialsError()
    })

    test('Success auth data, follow to MFA page', async ({page}) => {
        mfaRequiredPage = new MfaRequiredPage(page)

        await loginPage.fillCredentials(users.user_test.email, users.user_test.pwd)
        await loginPage.signIn()

        await mfaRequiredPage.checkInitState()
    })
})
