import { Page } from '@playwright/test';
import { EmailInput } from "./components/inputs/Email";
import { Button } from "./components/Button";
import { Control } from "./components/Control";
import { errors as email_errors } from "./consts/email";

export class ResetPasswordPage {
    readonly page: Page

    readonly emailInput: EmailInput

    readonly submitBtn: Button

    readonly emailErrSection: Control

    readonly title: Control

    constructor(page: Page) {
        this.page = page

        this.title = new Control({page: page, loc: page.locator('//h1[text()="Reset your password"]')})

        this.emailInput = new EmailInput(page)
        this.submitBtn = new Button({page: page, loc: page.locator('//button[@type="submit"]')})

        this.emailErrSection = new Control({page: page, loc: page.getByTestId('inputWrapper')})
    }

    async checkInitState() {
        await this.title.waitForVisible()
        await this.emailInput.waitForVisible()
        await this.submitBtn.waitForVisible()
    }

    async fillEmail(email: string) {
        await this.emailInput.fill(email)
    }

    async submitForm() {
        await this.submitBtn.click()
    }

    async seeEmailFormatErr() {
        await this.emailErrSection.waitForTextVisible(email_errors.ERROR_EMAIL_FORMAT)
    }
}
