import { Options } from "./options";
import { Locator, Page } from "@playwright/test";

export class Control {
    protected page: Page
    protected loc: Locator

    constructor(opts: Options) {
        this.page = opts.page
        this.loc = opts.loc
    }

    async waitForTextVisible(text: string) {
        await this.loc.getByText(text).waitFor({state: "visible"})
    }

    async waitForVisible() {
        await this.loc.waitFor({ state: "visible" })
    }

    async click() {
        await this.loc.click()
    }
}
